﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MigrationDemo.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}