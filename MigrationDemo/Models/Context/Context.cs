﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MigrationDemo.Models
{
    public class Context: ApplicationDbContext
    {
        public DbSet<Person>    Persons { get; set; }
        public DbSet<Book>      Books   { get; set; }
    }
}