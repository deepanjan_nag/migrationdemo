namespace MigrationDemo.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MigrationDemo.Models.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MigrationDemo.Models.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var persons = new[] {   new Person { Name="Deepanjan"},
                                    new Person { Name="Asha"},
                                    new Person { Name="Arihant"}};
            Array.ForEach(persons, p => context.Persons.AddOrUpdate(x => x.Name, p));
            context.SaveChanges();

            var books = new[] { new Book { Title="Hitch", Person=context.Persons.Single(p=>p.Name=="Arihant")},
                                new Book { Title="Star", Person=context.Persons.Single(p=>p.Name=="Arihant")},
                                new Book { Title="Buddha", Person=context.Persons.Single(p=>p.Name=="Asha")},
                                new Book { Title="Namyo", Person=context.Persons.Single(p=>p.Name=="Asha")},
                                new Book { Title="Suitable Boy", Person=context.Persons.Single(p=>p.Name=="Deepanjan")},
                                new Book { Title="Charles Dickens", Person=context.Persons.Single(p=>p.Name=="Deepanjan")}};

            Array.ForEach(books, b => context.Books.AddOrUpdate(x => new { x.Title, x.PersonId }, b));
            context.SaveChanges();
        }
    }
}
